## Wipe CI Cache

Sometimes a CI job gets in a weird state where old remnants in cache break an otherwise perfectly functioning repo. This is often the case with submodules, or when a package is moved to a new folder in GitLab.


If a package is moved, the old .git folder remains in cache pointing to the old location and gives a CI error such as this:

```
$ git clone -q --depth 1 https://gitlab.com/voxl-public/utilities/ci-tools
fatal: could not read Username for 'https://gitlab.com': No such device or address
Cleaning up file based variables 00:00
ERROR: Job failed: exit code 1
```

in the CI output, you should see something like this (around line 11)

```
Running on runner-idwdkaxk-project-19331258-concurrent-0 via threadripper
```

Log into the CI server as root and navigate to the `/var/lib/docker/volumes` directory. You will find one or more directories matching the cache for that CI job. In this case one was empty, the other contained the git clone for that job.

Remove everything from the `_data` subdirectory

```
root@threadripper:/var/lib/docker/volumes# rm -rf runner-idwdkaxk-project-19331258-concurrent-0-cache-c33bcaa1fd2c77edfc3893b41966cea8/_data/*
```

