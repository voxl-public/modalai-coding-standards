# ModalAI Inc. Coding standards


## Philosophy

Use of a ModalAI reference drone or breakout kit should require at most 3 steps

1. Flash a platform release (nightly or stable)
2. Configure (using voxl-configure-mpa)
3. Fly (or run CI test)

A feature isn't "done" until it is:

1. Merged to master/mainlined
2. Tagged
3. Integrated with the platform install
4. Building with CI every night
5. In the nightly platform release
6. Signed off by the Integration & Test team
7. Works out of the box
8. Documented on docs.modalai.com

DON'T:
 - Throw something in a branch and say it's working
 - Assume the rest of the world know your feature exists
 - Assume the rest of the world knows how to use your feature

To ensure code quality and catch regressions as soon as possible. Every night, CI builds an entire platform release for every ModalAI platform that includes a system image and the entire SDK that contains a single install.sh script to be run by customers. Everyone is a tester, everyone should be using the nightly platform release builds and checking for regressions.

When all desired features for a new stable SDK release are integrated into this nightly and validated to be working, we programmatically tag every source repo that was used to generate that nightly with an sdk-x.y tag and build a platform release for public distribution.

A Stable SDk release is just a point in time. Software keeps moving forward. The master branch of each repo keeps updating. Don't let features linger in branches and be forgotten and lost. Finish the feature, mainline it, and move on.

Installing a platform release (stable or nightly) should put any ModalAI reference drone or customer drone into a repeatable state. If a customer says they flashed the qrb5165-nightly-2023xxyy.tar nightly platform release and saw a certain behavior, we should be able to recreate that. To this end, we have the following rules:

- Every SDK version file needs a voxl-configure-XXXX tool in /usr/bin/ which should have both interactive modes (wizard) and non-interactive modes (cmd-line-args) that set up a service and its config files repeatably.
- That configure script should be called by voxl-configure-mpa
- All config files live in /etc/modlai/ and are wiped with the system image flash. They should also be forwards compatible to support software upgrades through apt without a system image flash.
- All hardware-specific calibration files live in /data/modalai/ and are not wiped during a platform release flash unless explicitly commanded by the user. They should also be forwards compatible to support software upgrades through apt without a system image flash.

While every source repo should have a README.md describing how it is build, the real golden truth describing how a package is build should be the .gitlab-ci.yml file. This tells the CI how to build and deploy the software programmatically. All software we test is made with CI. All software we distribute is made with CI. Random one-off binaries floating around copied and pasted from here to there are not reproducible and are a liability and a hazard. If it's not built by CI into the nightly platform release, it's not "done".

## Licensing

All public-facing source files shall at minimum contain "Copyright ModalAI Inc." at the top. However, this is overly restrictive and it is better to instead include the full license header which is a modified BSD license. Several pre-formatted examples are included here in this repo. The ModalAI license requires the software to be run on ModalAI hardware.

All public-facing projects shall contain the LICENSE file from this repository in the root directory as a "catch-all" in case source files omit the header accidentally or intentionally.

voxl-cross-template also contains a copy of this license and will be copied into other projects when running up update_other_repo.sh tool in voxl-cross-template.

To allow compatibility with GPL applications, two ModalAI libraries (libmodal-pipe and libmodal-json) are licensed under LGPL V3 instead of the ModalAI license. Copies of this license and headers are included in these coding standards to be copied into future projects that may require the LGPL license.


## Executable names

Executables (binaries, python scripts, bash scripts, etc) should be kebab-case and begin with "voxl-" so as to allow easy access from the command line.

Python and bash scripts may be placed in /usr/bin/ if flagged with the executable flag and stripped of the '.sh' or '.py' extension. eg:

* voxl-wifi-setup
* voxl-perfmon

All shell scripts should be bash compliant, no sh-only stuff.


## Git Contribution Identities

All commits to public gitlab should be from employee-specific ModalAI accounts so as to keep track of who is contributing to projects without needing names explicitly placed at the top of source files.


## Headers
C Header files should use doxygen blocks to describe functions and classes. There should also be a doxygen block at the top of each header file. Headers should also use include guards (#ifndef or #pragma once) and extern "C" lines to support C++ projects if the library is written in C. Example form libvoxl-io

```C
/**
 * <voxl_io.h>
 *
 * @brief      C interface for the VOXL apps proc to interface with IO
 *             ports through the SPLI (sensors DSP).
 *
 *
 * @author     James Strawson
 * @date       7/8/2019
 */

#ifndef VOXL_IO_H
#define VOXL_IO_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

.
.
.

/**
 * @brief     Sends data out the specified uart port through the SDSP
 *
 *            Requires configuring the port with voxl_uart_init first.
 *
 * @param[in] bus    The bus number /dev/tty-{bus} in DSPAL
 * @param[in] data   pointer to data to be sent
 * @param[in] bytes  number of bytes to send
 *
 * @return    returns number of bytes sent or -1 on error
 */
int voxl_uart_write(int bus, uint8_t* data, size_t bytes);

.
.
.

#ifdef __cplusplus
}
#endif

#endif // VOXL_IO_H
```

## Source code conventions

We use Google Coding standards except with 4-space wide indentations instead of 2.
It doesn't matter if you use spaces or tabs to indent, as long as you don't mix-and-match. When editing a file continue the convention used in that file.

James has whitespace explicitly highlighted in his text editor. If he opens one of your files and finds mixed tabs and spaces he will track you down....


## Building

Projects should be buildable with a public-facing docker environment specified at the top of the project README.
Each project should have the following build scripts in the root directory:

* clean.sh
	* cleans up build files and .ipk/.deb packages

* build.sh
	* builds project without cleaning first
	* may specify platform target with the following options where appropriate
		* apq8096
		* qrb5165
		* native

* make_package.sh
	* makes .ipk and/or .deb packages after building
	* should take an argument ipk or deb to specify which package to build
	* note we may remove ipk support entirely
	* the make_package.sh script in voxl-cross-template is designed to work for the vast majority of voxl-sdk packages without modification

* deploy_to_voxl.sh
	* push the just-generated ipk or deb to VOXL and install
	* the example in voxl-cross-template works for both packages and supports deploying over adb and ssh.
	* only applicable to projects designed to be installed on VOXL which is everything in the SDK gitlab directory.


The purpose of having consistency in these scripts and the build-environment dockers is to enable easy integration with gitlab-runner continuous integration. It also provides a consistent environment for the end-user.

voxl-cross-template should be used as a baseline example for these scripts:
* https://gitlab.com/voxl-public/other/voxl-cross-template

For more details, see the sdk-project-requirements.md document.



## Public Binary Package Repository

All packages made available on the package repository must be generated by Gitlab CI to ensure repeatability and a accountability.


The public repository exists at:
http://voxl-packages.modalai.com/dists/

Within "dists" we have directories for each platform:
 - APQ8096 ipk packages for VOXL1 and VOXLFlight
 - QRB5165 deb packages shared across all ModalAI compute boards with the QRB5165 chipset including the Qualcomm RB5 Flight.

Within each platform directory are individual repositories for each SDK release and internal development and testing branches.

* Most customers will want to stay on a constant stable package source.
* Others may wish to lock onto a specific voxl-sdk version release.
* Internally we will use dev and staging repos.

Each platform directory contains the following sections:

* stable
	* Is merely a symlink to the most recent stable sdk-vX.Y section.
	* Since SDK releases are tied to a sustem image, nobody should really use this.
	* Exists for CI to be able to pull from most recent stable release

* sdk-x.y
	* There will be many of these, one for every release.
	* These are frozen after an SDK release with only bugfixes allowed in.
	* See the below section on source-repo structure and tagging for how to push to these.
	* contains a voxl-suite_x.y.z_arm64.deb package with matching x&y numbers. z represents the patch number and bugfixes are backported to this SDK release.
	* push to this branch by making an sdk-x.y.z tag on any branch

* dev
	* contains the latest packages built every time a commit is pushed to the 'dev' branch.
	* For internal use by the SDK team for bringing up new features only. This may often contain broken packages!
	* Things should be moved out of dev into staging ASAP

* staging
	* This is for preparing for the next voxl-sdk release.
	* Used by the "integration & test" team before a voxl-sdk release.
	* Push to this repo by merging to the `master` branch and making a tag of the form vX.Y.Z matching the deb package version


After installing a platform release to VOXL, the following file will be set to point to either an sdk-x.y repo (for stable platform released) or the staging repo (for nightly platform releases)

```
/etc/apt/sources.list.d/modalai.list
```

This file will contain a line similar to the following:

```
deb [trusted=yes] http://voxl-packages.modalai.com/ qrb5165 sdk-1.0
```

APT does not support numbers in the section name for tree structures. This may be due to (or a reason for) the typical use of alphabetical codenames in software releases such as Bionic/Xenial. However a customer can still specify a specific SDK version repo via URL:

```
deb [trusted=yes] http://voxl-packages.modalai.com/qrb5165/sdk-1.0/stable
```

More details: https://wiki.debian.org/DebianRepository/Format



### Debian Package Naming

Packages must be named name_version_arch.deb. Packages may not contain underscores in the name. All packages targeted for install on VOXL should have the arm64 architecture even if it contains 32-bit binaries. This Architecture tag is really designed for package organization in large repos targeting different platforms.

A package version consists of a normal x.y.z major minor patch sequence, plus an optional suffix. dev packages contain a date/time suffix generated by the make_package.sh script by using the --timestamp argument.

Examples:

* libmodal-json_0.4.0-202202080557_arm64.deb
	* This is a dev package with a timestamp suffix in the dev repo
* libmodal-json_0.4.0_arm64.deb
	* Normal stable release for stable or staging.
* libmodal-json_0.4.0+sdk13u1_arm64.deb
	* bugfix for an already releases voxl-sdk V1.3 package


### Debian Package Versioning

Good overview here: https://www.youtube.com/watch?v=WbIKdPdVcSU

* Minus "-" indicates a derivative
	* Debian always uses a single digit, every Debian package looks like 1.2.3-1
	* Ubuntu clones these packages sometime, other times they make their own derivative
	* e.g. Ubuntu changes a logo to their own, the package is now 1.2.3-1ubuntu1

* Tilde "~" sets this release to be a lower priority than a normal package
	* usually for 3rd party repos that duplicate packages from an official repo
	* assuming the official repo contains 1.0.0, a new package 1.2.0~ppa1 can be made
	* user can select the ppa1 release if they want, but apt will prefer the official release during automatic upgrades.
	* can also be used to indicate release candidates or beta releases

* Plus "+" is the opposite of Tilde, it indicates the package should take priority.
	* used for bugfixes that can't wait for the next upstream version release.
	* e.g. 1.0.0+sdk1.3 takes priority over the normal Debian 1.0.0-1 release
	* once the fix is released upstream, go back to 1.0.1-1
	* we use this for maintenance patches!

* Epoch Prefix (2:1.0.2)
	* Used to fix version numbering mistakes. e.g. a package was uploaded with V1.2.0 but was supposed to be 1.0.2
	* can't upload the correct v1.0.2 package because APT will prefer the "newer" 1.2.0 release.
	* add an Epoch prefix '2:' to force apt to upgrade to the correct package.
	* Should not be needed at ModalAI, but valid if necessary.


### Debian Package Repo Tree Structure

The ModalAI Debian repository tree matches the structure used by larger projects such as Ubuntu, Debian, Docker, etc. This tree consists of 3 layers:

1. Distribution
	* we use this to designate the hardware platform: QRB5165 or APQ8096
	* this could be expanded to support desktop packages if needed
2. Section
	* This is usually main/contrib/non-free on Debian Systems
	* Docker uses this to separate nightly/stable/test packages
	* We use this to separate voxl-sdk releases, more details below
3. Architecture
	* For simplicity, all VOXL packages must be tagged with the amd64 architecture regardless of the actual contents.
	* This flag is just to allow APT to choose the correct sections of the repository to pull packages from.

Currently we do NOT sign packages. Should we? It's likely more of an annoyance than a benefit.




### New SDK release procedure

Assume V1.0 is the current stable, and V1.1 is in staging. The following steps are taken:

1. Software team tests upgradability from stable to staging.
2. Testing team points their apt sources to staging and validates performance.
3. Software team duplicates sdk-v1.1 into a new repo sdk-1.2
4. Software team moves symlinks stable and staging to sdk-v1.1 and sdk-v1.2
5. Software team generates a new voxl-platform release V1.1
6. Production team starts using the new platform release.




## Git Branching and Tagging Rules

All packages on the public binary repo are generated via CI based on branches and tags. This is done by the common ci-tools stages:

* https://gitlab.com/voxl-public/utilities/ci-tools



### Branches

Small changes can be made straight to dev. Checked for function and buildability, then merged to master and tagged with a vx.y.z tag which commands CI to send a new package version to the staging branch.

Medium changes that take a few days and require multiple packages being modified should be done by modifying the dev repo for each of the relevant packages. Once all of the packages required for the feature have passed initial sanity checks by the developer, they should all be merged to master and tagged with a vx.y.z tag to send all the packages to staging for testing by the Integration & Test team.

Large changes spanning more than a week should live in a personal branch not built by CI.

More details on each gitlab source repo:

* master
	* This is what the public usually looks at when browsing source code.
	* It should at a minimum remain buildable, preferably functional.
	* Generally reflects what is in the staging binary repo section.
	* This branch is built by CI on a nightly basis and with every commit.
	* However, these CI builds are NOT pushed to the binary repo without a tag.
	* Don't use "main" for the default branch, it must be called "master" other scripts depend on this.
	* Every night a platform release is built by CI based on the latest vx.y.z tag on master branch from every SDK repo.
	* making an vx.y.z tag on master is how you signal to the rest of the modalai team you have something new to test and evaluate.


* dev
	* Every commit to dev will push a new package to the public dev repository
	* There is no expectation for dev to be stable, but please make an effort to keep it functional.
	* Please do feature development that is expected to take more than 1 commit on a personal or feature branch.
	* Don't let things sit in dev too long or it won't get integrated into nightly platform releases or tested by the integration and test team. A feture isn't "done" until it's tagged, in the master branch, tested, and integrated.


* Personal and feature branches
	* used for development of new features.
	* merge into dev first when the feature is complete.
	* please keep the git tree clean, delete personal and feature branches when done.
	* name the branch with your first name or feature name for identifiability.
	* Don't let things sit in dev too long or it won't get integrated into nightly platform releases or tested by the integration and test team. A feture isn't "done" until it's tagged, in the master branch, tested, and integrated.



### Tags

Tags signify when a package is ready for public use and indicate to CI to push the compiled result to a public binary repo. Generally tags should be made on the master branch, however, maintenance releases for old SDK versions should be on separate maintenance branches.

To push a package to the staging binary repo, make a tag of the form `vx.y.z`. This should be done when new features are complete and merged into the master branch of a project. The package version must match what's in the package pkg/config file! It may contain a suffix in the case of backported bugfixes or derivative packages such as voxl-opencv.


To push a package to the current stable or older sdk-x.y repositories, make a tag of the form `sdk-x.y.z`. This means that for every new voxl-sdk release, every single package must be tagged again, even when no changes have been made. (Alex G will script this procedure so it doesn't take much time). The advantage is that each git source repo contains a complete history of what went into every voxl-sdk release. A version number or description may optionally be added to the end of the tag in the case of backporting bugfixes.

We encourage tagging heavily. Tagging is how you should describe significant points in time such as a stable SDK release. Software always moves forward and always changes, new stuff should always get mainlined ASAP. People can always go back to a tag.



### Maintenance of Old SDK Releases

If an important customer is sticking with a previous voxl-sdk release and finds a bug we must fix, the following procedure takes place:

1. Git checkout the last tag ending in their sdk-x.y version.
2. Make a new branch called sdk-x.y.z if the current master would break the old SDK
3. Make the fix.
4. Tag the fix with sdk-x.y.z to reflect the new voxl-suite version
5. update voxl-suite with the incremented z version
6. make sure the new package made its way to the sdk-x.y deb repo
7. build new platform releases with the platform-tools repo toolkit
8. distribute the new platform release to the production team and integration&test team for validation
9. Upload new platform release to the public downloads section and robot framework for production



